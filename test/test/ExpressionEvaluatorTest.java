package test;


import expressionevaluator.Multiplication;
import expressionevaluator.Expression;
import expressionevaluator.Addition;
import expressionevaluator.Constant;
import expressionevaluator.BinaryOperator;
import org.junit.Assert;
import org.junit.Test;



public class ExpressionEvaluatorTest {
    
    @Test
    public void constantExpressionTest(){
        Assert.assertEquals(new Constant(1).calculate(),1);
    }
    
    @Test
    public void otherConstantExpressionTest(){
        Assert.assertEquals(new Constant(2).calculate(),2);
    }
    
    @Test
    public void complexExpressionConstantsTest(){
        Expression expression= new Addition(new Constant(1),new Constant(3));
        Assert.assertEquals(expression.calculate(),4);
    }
    
    @Test
    public void complexExpressionExpressionsTest(){
        Expression expression= new Addition(new Multiplication(new Constant(2), new Constant(3)),new Constant(3));
        Assert.assertEquals(expression.calculate(),9);
    }
    
    public class IntegerAddition extends BinaryOperator{

        @Override
        public Object execute(Object left, Object right) {
            return (Integer) left + (Integer) right;
        }
    
    
    }
    
    
    
    
    
}
