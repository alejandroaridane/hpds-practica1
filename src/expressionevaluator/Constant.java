/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package expressionevaluator;

/**
 *
 * @author usuario
 */
public class Constant implements Expression {
    final int value;

    public Constant(int value) {
        this.value = value;
    }

    @Override
    public Object calculate() {
        return value;
    }
    
}
